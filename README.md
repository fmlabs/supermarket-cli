# Supermarket App
This app is a Supermarket simulation app that will have these kind of flow :

1. Display greetings and show some promotional items and its price
2. Take the user input whether they want promotional items or show all items that are available
in the supermarket
3. Then show the result according to user requests
4. Finally take the input of what the user wants to buy ( could be 1 item or more ), calculate the
total price and display the item details and total price to the console

## Example Result
### Case 1

user input -> promotional items -> buy 3 masker and 2 susu -> display total price and details

expected output:
```
Total harga: Rp.175.000
Detail:
2 susu -> Rp.100.000
3 masker -> Rp.75.000
```
**Result**

![Screen Shot 2022-04-05 at 20.40.17.png](./Screen Shot 2022-04-05 at 20.40.17.png)

### Case 2

user input -> all items -> buy 2 lampu, 4 apel and 1 susu -> display total price and details

expected output:
```
Total harga: Rp.200.000
Detail:
2 lampu -> Rp.30.000
4 apel -> Rp.120.000
1 susu -> Rp.50.000
```
**Result**

![Screen Shot 2022-04-05 at 20.40.56.png](./Screen Shot 2022-04-05 at 20.40.56.png)

## How to Use
### Run the App

```
python app.py
```

It will show this output
```
================================
Welcome to Supermarket IndoAlpa
================================
Berikut adalah barang yang sedang promo:
1. susu 50000
4. masker 25000
Apakah anda ingin membeli barang promo atau lihat semua barang yang tersedia?
```

### Run the unittest
```
python -m unittest -v
```

it will output the result like this

```
test_non_promo_order (tests.test_order.TestOrder)
Test Case 2 ... ok
test_promo_order (tests.test_order.TestOrder)
Test Case 1 ... ok

----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

## Prerequisites
- Python 3.8.2
