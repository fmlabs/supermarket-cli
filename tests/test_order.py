import unittest

from app.process import order
from app.models import cart

susu, daging, lampu, masker, apel = 1, 2, 3, 4, 5,


class TestOrder(unittest.TestCase):
    def test_promo_order(self):
        """
        Test Case 1

        user input -> promotional items -> buy 3 masker and 2 susu -> display total price and details
        expected output:

        Total harga: Rp.175.000
        Detail:
        2 susu -> Rp.100.000
        3 masker -> Rp.75.000
        """
        cart.clear_cart()
        order.add_order(susu, 2)
        order.add_order(masker, 3)
        total, _ = order.checkout_order()
        self.assertEqual(total, 175000)

    def test_non_promo_order(self):
        """
        Test Case 2

        user input -> all items -> buy 2 lampu, 4 apel and 1 susu -> display total price and details
        expected output:

        Total harga: Rp.200.000
        Detail:
        2 lampu -> Rp.30.000
        4 apel -> Rp.120.000
        1 susu -> Rp.50.000
        """
        cart.clear_cart()
        order.add_order(lampu, 2)
        order.add_order(apel, 4)
        order.add_order(susu, 1)
        total, _ = order.checkout_order()
        self.assertEqual(total, 200000)


if __name__ == '__main__':
    unittest.main()
