cart = {}


def add_to_cart(item_code, item_order, item_count):
    """
    Add item to cart
    """
    if item_code not in cart:
        cart[item_code] = {
            "item": item_order,
            "count": 0,
            "subtotal": 0
        }

    last_value = cart[item_code]
    item_count = last_value["count"]+item_count
    subtotal = item_order["harga"] * item_count
    cart[item_code].update({
        "count": item_count,
        "subtotal": subtotal
    })


def clear_cart():
    """
    Clear cart
    """
    cart.clear()
