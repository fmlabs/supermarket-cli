# Item catalogs
items = {
    1: {
        "item": "susu",
        "harga": 50000,
        "is_promo": True
    },
    2: {
        "item": "daging",
        "harga": 20000,
        "is_promo": False
    },
    3: {
        "item": "lampu",
        "harga": 15000,
        "is_promo": False
    },
    4: {
        "item": "masker",
        "harga": 25000,
        "is_promo": True
    },
    5: {
        "item": "apel",
        "harga": 30000,
        "is_promo": False
    }
}


def get_promo_items():
    """
    Get item with promo flag
    """
    promo_items = {}
    for item_code, item in items.items():
        if item.get("is_promo"):
            promo_items[item_code] = item
    return promo_items


def get_all_items():
    """
    Get all item available
    """
    return items
