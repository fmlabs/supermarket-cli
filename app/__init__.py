from app.process import order
from app.models import items


def run():
    """
    Run the application
    """

    # Greetings
    print("================================")
    print("Welcome to Supermarket IndoAlpa")
    print("================================")

    available_items = items.get_promo_items()
    print("Berikut adalah barang yang sedang promo:")
    print_item(available_items)
    print("Apakah anda ingin membeli barang promo atau lihat semua barang yang tersedia?")
    print("[1] Beli barang promo")
    print("[2] Lihat semua barang")
    print("[0] Keluar")
    user_input = int(input())
    if user_input == 2:
        available_items = items.get_all_items()
        print_item(available_items)
    elif user_input == 0:
        exit()

    # Order process
    order_checkout = False
    while order_checkout is False:
        print("Masukkan kode barang yang anda mau atau ketik 0 untuk checkout pesanan:")
        user_order_item = int(input())
        if user_order_item == 0:
            order_checkout = True
        else:
            if user_order_item not in available_items:
                print("Kode barang salah atau tidak tersedia, silahkan coba lagi!")
            else:
                print("Masukkan jumlah item:")
                user_order_item_count = int(input())
                order.add_order(user_order_item, user_order_item_count)

    total, cart = order.checkout_order()
    print("Total harga : Rp {}".format(total))
    print("Detail : ")
    print_cart(cart)


def print_item(items):
    """
    Print available item
    """
    for item_code, item in items.items():
        print("{}. {} {}".format(item_code, item["item"], item["harga"]))


def print_cart(cart):
    """
    Print cart details
    """
    for item in cart.values():
        print("{} {} -> {}".format(item["count"], item["item"]["item"], item["subtotal"]))
