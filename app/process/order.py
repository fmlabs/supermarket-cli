from app.models import cart, items


def add_order(item_code, item_count):
    """
    Add item to cart
    """
    selected_item = items.items.get(item_code)
    cart.add_to_cart(item_code, selected_item, item_count)


def checkout_order():
    """
    Checkout cart
    """
    total = 0
    for item in cart.cart.values():
        total += item["subtotal"]
    return total, cart.cart
